import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import QtWebSockets 1.1
import QtQuick.Controls.Material 2.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Extras 1.4
import Qt.labs.settings 1.0
import "fonts/MaterialDesign.js" as Icons
import "scripts/colors.js" as Colors
import "scripts/localstorage.js" as Storage
import "scripts/listview.js" as SocketInterop
import QtQuick.LocalStorage 2.12
import QtQuick.Dialogs 1.2
import absence.dev.crypto 1.0
import absence.dev.messaging 1.0
import absence.dev.io 1.0

import QtQuick3D 1.15
import QtQuick3D.Materials 1.15

ApplicationWindow {
    id: applicationWindow
    width: Screen.desktopAvailableWidth / 2
    height: Screen.desktopAvailableHeight / 2
    visible: true
    title: qsTr("Absence Dev")

    Material.theme: Material.Dark
    Shortcut{
        id: addNewSocketShortcut
        sequence: "Ctrl+Shift+a"
        onActivated: addNewSocketDrawer.open()
    }

    Shortcut{
        id: quitApplicationShortcut
        sequence: "Ctrl+Shift+q"
        onActivated: Qt.quit()
    }

    Shortcut{
        id: removeSocketShortcut
        sequence: "Ctrl+Shift+r"
        onActivated: confirmDeleteDialog.open()
    }

    Shortcut{
        id: settingsShortcut
        sequence: "Ctrl+Shift+s"
        onActivated: settingsDrawer.open()
    }

    FileIO{
        id: fileIO
        onFileRead: Sockets.addFromJson(content)
    }

    FileDialog{
        id: importFileDialog
        folder: shortcuts.home
        title: "Import Socket"

        onAccepted: fileIO.read(importFileDialog.fileUrl)
        onRejected: importFileDialog.close()
    }

    FileDialog{
        id: exportFileDialog
        folder: shortcuts.home
        selectExisting: false
        title: "Export Socket"

        onAccepted: fileIO.write(JSON.stringify(SocketInterop.getCurrentModelForSocketListView()), exportFileDialog.fileUrl)
        onRejected: exportFileDialog.close()
    }


    Crypto{
        id: crypto
    }

    LocalMessenger{
        id: localMessenger
        onLocalMessageReceived: messageEditor.addMessage(message)
    }

    ConfirmDeleteDialog {
        id: confirmDeleteDialog
        onConfirm: {
            var currentIndex = socketListView.currentIndex
            var currentModel = socketListView.model
            var selectedObject = currentModel.get(currentIndex)
            Storage.removeSocket(selectedObject)
            socketListView.repopulate()
            close()
        }
    }

    WebSocket{
        id: webMessagingSocket
        onTextMessageReceived: {
            var currentIndex = socketListView.currentIndex
            var currentModel = socketListView.model
            var selectedObject = currentModel.get(currentIndex)

            messageEditor.addMessage(message)
        }

        onStatusChanged: {
            if (webMessagingSocket.status == WebSocket.Error) {
                console.log("Error: " + webMessagingSocket.errorString)
            } else if (webMessagingSocket.status == WebSocket.Open) {

            } else if (webMessagingSocket.status == WebSocket.Closed) {

            }
        }
        active: true
    }

    Settings{
        id: appSettings
        property string username
        property string localPassword
        property bool localMode
        property string localPort : "45454"
        property string localAddress : "239.255.43.21"

        onLocalPortChanged: localMessenger.setPort(localPort)
        onLocalAddressChanged: localMessenger.setGroupAddress(localAddress)
    }

    Component.onCompleted: {
        localMessenger.setPort(appSettings.localPort)
        localMessenger.setGroupAddress(appSettings.localAddress)
    }

    FontLoader{
        source: "fonts/MaterialIcons-Regular.ttf"
    }

    FontLoader{
        id: openMoji
        source: "fonts/OpenMoji-Color.ttf"
    }

    Drawer{
        id: settingsDrawer
        edge: Qt.RightEdge
        height: applicationWindow.height
        width: parent.width * .35
        SettingsSidePanel {
            id: settingsListView
        }
    }

    Drawer{
        id: socketInfoDrawer
        edge: Qt.RightEdge
        height: applicationWindow.height
        width: parent.width * .35
        SocketInfoSidePanel{
            id: infoSidePanel
        }

        signal setProperties(var input)
        onSetProperties: infoSidePanel.setProperties(input)
    }

    Drawer{
        id: addNewSocketDrawer
        edge: Qt.RightEdge
        height: parent.height
        width: parent.width * .35

        AddNewSocket{
            onAddClicked: {
                addNewSocketDrawer.close()
                socketListView.repopulate()
            }
        }
    }

    function tryLogin(){
        var modelHash = SocketInterop.getCurrentModelForSocketListView()
        var currentPw = passwordTextField.text
        if(crypto.validatePasswordHash(currentPw, modelHash.password)){
            crypto.setPassword(currentPw)
            messageEditor.localMode = false
            appSettings.localMode = false
            webMessagingSocket.url = modelHash.endpoint

            passwordPromptPopup.close()
            messageEditor.focusEditor()
            applicationWindow.setTitle("socket - " + modelHash.name)
            messageEditor.clearMessages()

        }else{
            errorLabel.text = Colors.toColor("Invalid Password", "red")
        }

        passwordTextField.text = ""
    }

    function sendMessageToEditor(textMessage){
        var selectedObject = SocketInterop.getCurrentModelForSocketListView()
        var payload = JSON.stringify({
                                         user: appSettings.username || 'anonymous',
                                         message: textMessage
                                     })
        if(appSettings.localMode){
            localMessenger.sendTextMessage(payload)
        }else{
            var encryptedPacket = crypto.encrypt(payload)
            webMessagingSocket.sendTextMessage(encryptedPacket)
        }
    }

    Popup{
        id: passwordPromptPopup
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        parent: Overlay.overlay
        modal: true
        anchors.centerIn: parent
        width: parent.width / 4
        contentItem: ColumnLayout{
            anchors.centerIn: parent
            Layout.fillWidth: true
            Layout.fillHeight: true
            Label{
                id: authRequiredLabel
                text: qsTr("Authentication Required")
            }
            Label{
                id: errorLabel
            }

            TextField{
                id: passwordTextField
                width: parent.width
                Layout.fillWidth: true
                placeholderText: qsTr("Enter Password")
                passwordCharacter: "*"
                echoMode: "Password"


                Keys.onPressed: {
                    if(event.key === 16777220 || event.key === 16777221){
                        tryLogin()
                    }
                }
            }

            Button{
                width: parent.width
                Layout.fillWidth: true
                text: "Login"
                onClicked: tryLogin()
            }
        }
    }

    RowLayout {
        anchors.fill: parent
        spacing: 2
        SocketsListView {
            id: socketListView
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.minimumWidth: applicationWindow.width * .25
        }

        MessageEditor {
            id: messageEditor
            color: Material.dropShadowColor
            onEditorKeyPressed: sendMessageToEditor(message)
        }
    }
}
