import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import QtWebSockets 1.1
import QtQuick.Controls.Material 2.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Extras 1.4
import Qt.labs.settings 1.0
import "fonts/MaterialDesign.js" as Icons
import "scripts/localstorage.js" as Storage
import "scripts/colors.js" as Colors
import QtQuick.LocalStorage 2.12

ListView {
    id: socketListView
    signal disableAllSocketItems()
    signal repopulate()

    Layout.fillHeight: true
    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
    onDisableAllSocketItems: currentIndex = -1
    focus: true
    ScrollBar.vertical: ScrollBar {
        visible: false
    }

    function populateModel(){
        socketListViewModel.clear()
        socketListViewModel.append({
                         name: 'Local Messaging',
                         description: 'WiFi Messaging',
                         endpoint: 'local',
                         password: ''
                     })
        for(var item of Storage.getAllSockets()){
            socketListViewModel.append(item)
        }
    }
    onRepopulate: populateModel()

    model: ListModel{
        id: socketListViewModel
        Component.onCompleted: populateModel()
    }
    
    delegate: Item{
        width: parent.width
        height: 50

        ColumnLayout{
            anchors.fill: parent
            Label{
                text: name
                font.pointSize: 10
                leftPadding: 8
            }

            Label{
                text: description
                font.pointSize: 8
                leftPadding: 8
            }

            Rectangle{
                width: parent.width
                height: 1
                color: "grey"
            }
        }
        
        Keys.onPressed: {
            if(event.key === 16777223) confirmDeleteDialog.open()
            if(event.key === 16777220 || event.key === 16777221) {
                socketListView.currentIndex = index
                if(endpoint !== 'local'){
                    passwordPromptPopup.open()
                }else{
                    messageEditor.localMode = true
                    appSettings.localMode = true
                    webMessagingSocket.active = false
                }
            }
        }

        MouseArea{
            anchors.fill: parent
            acceptedButtons: Qt.RightButton | Qt.LeftButton
            onDoubleClicked: Storage.infoOnSocket()

            onPressAndHold: contextMenu.popup()

            onClicked: {
                if (mouse.button == Qt.RightButton) contextMenu.popup()
                if (mouse.button == Qt.LeftButton) {
                    socketListView.currentIndex = index
                    if(endpoint !== 'local'){
                        passwordPromptPopup.open()
                    }else{
                        messageEditor.localMode = true
                        appSettings.localMode = true
                        webMessagingSocket.active = false
                    }
                }
            }
            Menu {
                id: contextMenu
                MenuItem {
                    text: "Info"
                    onClicked: Storage.infoOnSocket()
                }
                MenuItem {
                    text: "Remove"
                    onClicked: confirmDeleteDialog.open()
                }
                MenuItem{
                    text: "Export"
                    onClicked: exportFileDialog.visible = true
                }
            }
        }
    }
    
    highlight: Rectangle{
        color: Material.highlightedButtonColor
    }

    footerPositioning: ListView.OverlayFooter
    footer: RowLayout{
        anchors.centerIn: parent
        Layout.fillWidth: true
        height: 50
        ToolButton{
            text: Icons.icons.settings
            onClicked: settingsDrawer.open()
        }
        ToolButton{
            text: Icons.icons.note_add
            onClicked: addNewSocketDrawer.open()
        }
        ToolButton{
            text: Icons.icons.file_upload
            onClicked: importFileDialog.visible = true
        }
    }
}
