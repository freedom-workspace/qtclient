import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import QtWebSockets 1.1
import QtQuick.Controls.Material 2.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Extras 1.4
import Qt.labs.settings 1.0
import "fonts/MaterialDesign.js" as Icons
import "scripts/localstorage.js" as Storage
import "scripts/colors.js" as Colors
import QtQuick.LocalStorage 2.12

ColumnLayout{
    signal addClicked()
    spacing: 2
    anchors.fill: parent
    Label{
        text: qsTr("Add Socket")
        Layout.maximumHeight: 50
        Layout.alignment: Qt.AlignHCenter
    }

    TextField{
        id: name
        placeholderText: qsTr("Name")
        Layout.maximumHeight: 50
        Layout.fillWidth: true
        Layout.leftMargin: 4
        Layout.rightMargin: 4
    }

    TextField{
        id: description
        placeholderText: qsTr("Description")
        Layout.maximumHeight: 50
        Layout.fillWidth: true
        Layout.leftMargin: 4
        Layout.rightMargin: 4
    }


    TextField{
        id: endpoint
        placeholderText: qsTr("Endpoint")
        Layout.maximumHeight: 50
        Layout.fillWidth: true
        Layout.leftMargin: 4
        Layout.rightMargin: 4
    }

    TextField{
        id: password
        placeholderText: qsTr("Password")
        Layout.maximumHeight: 50
        Layout.fillWidth: true
        passwordCharacter: "*"
        echoMode: "Password"
        Layout.leftMargin: 4
        Layout.rightMargin: 4
    }

    Button{
        text: qsTr("Add")
        Layout.maximumHeight: 50
        Layout.fillWidth: true
        onClicked: {
            Storage.addSocket(name.text, description.text, endpoint.text, password.text)
            addClicked()
        }
        Layout.leftMargin: 4
        Layout.rightMargin: 4
    }

    Item{
        Layout.fillHeight: true
    }
}
