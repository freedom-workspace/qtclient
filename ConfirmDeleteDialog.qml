import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import QtWebSockets 1.1
import QtQuick.Controls.Material 2.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Extras 1.4
import Qt.labs.settings 1.0
import "scripts/colors.js" as Colors
import QtQuick.LocalStorage 2.12
import QtQuick.Dialogs 1.2

Popup{
    id: confirmDeleteDialog

    signal confirm()
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
    parent: Overlay.overlay
    modal: true
    anchors.centerIn: parent
    contentItem: ColumnLayout{
        Label{
            text: qsTr("Are You Sure?")
            Layout.maximumHeight: 50
            Layout.alignment: Qt.AlignHCenter
        }
        RowLayout{
            Button{
                text: Colors.toColor('Yes', "green")
                Layout.maximumHeight: 50
                Layout.maximumWidth: parent.width / 2
                onClicked: confirm()
            }
            Button{
                text: Colors.toColor('No', "red")
                Layout.maximumHeight: 50
                Layout.maximumWidth: parent.width / 2
                onClicked: confirmDeleteDialog.close()
            }
        }
    }
}
