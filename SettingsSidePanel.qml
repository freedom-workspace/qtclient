import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import QtWebSockets 1.1
import QtQuick.Controls.Material 2.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Extras 1.4

//RowLayout{
//    width: parent.width
//    height: parent.height

//    Label{
//        text: qsTr("Settings")
//        Layout.alignment: Qt.AlignHCenter
//    }

//    ColumnLayout{
//        width: parent.width
//        Label{
//            text: "username"
//            Layout.fillWidth: true
//            topPadding: 4
//        }

//        TextField{
//            Layout.fillWidth: true
//            Layout.maximumHeight: 50
//            width: parent.width
//            onTextEdited: appSettings.username = text
//            text: appSettings.username
//        }
//    }

//    ColumnLayout{
//        width: parent.width
//        Label{
//            text: "Organization Name"
//            Layout.fillWidth: true
//            topPadding: 4
//        }

//        TextField{
//            Layout.fillWidth: true
//            Layout.maximumHeight: 50
//            width: parent.width
//            onTextEdited: appSettings.orgName = text
//            text: appSettings.orgName
//        }
//    }

//    ColumnLayout{
//        width: parent.width
//        Label{
//            text: "Company Name"
//            Layout.fillWidth: true
//            topPadding: 4
//        }

//        TextField{
//            Layout.fillWidth: true
//            Layout.maximumHeight: 50
//            width: parent.width
//            onTextEdited: appSettings.appName = text
//            text: appSettings.appName
//        }
//    }
//}

ListView {
    id: settingsListView

    width: parent.width
    height: parent.height

    antialiasing: true
    spacing: 2
    snapMode: ListView.SnapOneItem
    highlightRangeMode: ListView.ApplyRange
    leftMargin: 4
    rightMargin: 4
    header: Label{
        topPadding: 4
        text: qsTr("Settings")
        anchors.centerIn: parent
        bottomPadding: 4
    }

    model: ListModel {
        ListElement {
            name: "Username"
            key: "username"
        }
        ListElement{
            name: "Local Port"
            key: "localPort"
        }
        ListElement{
            name: "Local Address"
            key: "localAddress"
        }
    }
    delegate:  ColumnLayout{
        width: parent.width
        Label{
            text: name
            Layout.fillWidth: true
            topPadding: 4
        }

        TextField{
            id: inputField
            Layout.fillWidth: true
            Layout.maximumHeight: 50
            width: parent.width

            onTextEdited: appSettings.setValue(key, text)
            text: appSettings.value(key)
        }
    }
    Component.onCompleted: {
    }
}
