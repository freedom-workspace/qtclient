import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import QtWebSockets 1.1
import QtQuick.Controls.Material 2.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Extras 1.4
import Qt.labs.settings 1.0
import "fonts/MaterialDesign.js" as Icons
import "scripts/localstorage.js" as Sockets
import "scripts/colors.js" as Colors
import "scripts/listview.js" as SocketInterop
import QtQuick.LocalStorage 2.12
import QtQuick.Dialogs 1.2
import absence.dev.crypto 1.0
import absence.dev.messaging 1.0
Rectangle {
    property bool localMode
    signal clearMessages()
    signal focusEditor()
    signal addMessage(string message)
    signal editorKeyPressed(string message)

    id: messageEditor
    Layout.fillWidth: true
    Layout.fillHeight: true
    
    onClearMessages: messageListView.model.clear()
    onFocusEditor: messageEditorTextField.forceActiveFocus()
    onAddMessage: {
        if(localMode){
            messageListView.model.append(JSON.parse(message))
        }else{
            var selectedObject = SocketInterop.getCurrentModelForSocketListView()
            var packet = crypto.decrypt(message)
            var decodedObject = JSON.parse(packet)
            messageListView.model.append(decodedObject)
        }
    }

    ColumnLayout{
        anchors.fill: parent
        Layout.fillWidth: true
        Layout.fillHeight: true

        ListView{
            id: messageListView
            Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.rightMargin: parent.width / 4
            Layout.leftMargin: 4
            topMargin: 4
            bottomMargin: 2
            spacing: 4
            ScrollBar.vertical: ScrollBar {
                visible: false
            }

            model: ListModel{}

            focus: true
            delegate: ColumnLayout{
                Layout.fillWidth: true
                Label{
                    text: Colors.toColor(user, Material.accent)
                    Layout.alignment: Qt.AlignLeft
                    font.pointSize: 8
                    leftPadding: 16
                }
                Label{
                    width: parent.width
                    text: Colors.toColor(message, Material.accent)
                    Layout.alignment: Qt.AlignLeft
                    font.pointSize: 10
                    rightPadding: 4
                    leftPadding: 16
                    wrapMode: "WordWrap"
                }
            }
        }

        RowLayout{
            Layout.maximumHeight: 50
            Layout.minimumHeight: 35
            Layout.leftMargin: 8
            Layout.rightMargin: 8
            TextField{
                id: messageEditorTextField
                Layout.fillWidth: true
                placeholderText: qsTr("Message")
                wrapMode: "WordWrap"

                Keys.onPressed: {
                    if(event.key === 16777220 || event.key === 16777221){
                        editorKeyPressed(text)
                        clear()
                    }
                }
            }
        }
    }
}
