#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtWebSockets/QWebSocket>
#include <QQuickStyle>
#include "includes/FileIO.h"
#include "includes/Crypto.h"
#include "includes/LocalMessenger.h"
#include <QDebug>
void registerTypes()
{
    qmlRegisterType<Crypto>("absence.dev.crypto",1,0,"Crypto");
    qmlRegisterType<LocalMessenger>("absence.dev.messaging",1,0,"LocalMessenger");
    qmlRegisterType<FileIO>("absence.dev.io",1,0,"FileIO");
}

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QQuickStyle::setStyle("Material");
    QCoreApplication::setOrganizationName("CYBORG");
    QCoreApplication::setApplicationName("ADEV");

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    registerTypes();
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);
    return app.exec();
}
