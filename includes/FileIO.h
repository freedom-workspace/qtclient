#ifndef FILEIO_H
#define FILEIO_H

#include <QObject>

class QString;
class FileIO : public QObject
{
    Q_OBJECT
public:
    explicit FileIO(QObject *parent = nullptr);
public slots:
    void write(QString content, QString path);
    QString read(QString path);
private slots:
signals:
    void fileWritten();
    void fileRead(QString content);
private:
};

#endif // FILEIO_H
