#ifndef CRYPTO_H
#define CRYPTO_H

#include <QObject>
#include <qqml.h>
#include <QString>
#include <QMap>
#include <QFile>
class QString;
class QByteArray;
class Crypto : public QObject
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit Crypto(QObject *parent = nullptr);
    ~Crypto();
    Q_INVOKABLE QString encrypt(QString input);
    Q_INVOKABLE QString decrypt(QString input);
    Q_INVOKABLE QString generatePasswordHash(QString input);
    Q_INVOKABLE bool validatePasswordHash(QString entry, QString hash);
public slots:
    void regenerateKey();

    void setPassword(QString password){
        m_password = password;
    }

    void clearPassword(){
        m_password = "";
    }
signals:
private:
    void createAppKey(QFile&);
    void readOrGenerateAppKey();
    QByteArray applicationKey;

    const int ApplicationKeySize = 2048;
    const int KeyGenIterations = 4;
    const int HashP = 1;
    const int HashM = 1024;
    const int HashT = 1;

    QString m_password;
};
#endif // CRYPTO_H
