#ifndef LOCALMESSENGER_H
#define LOCALMESSENGER_H

#include <QObject>
#include <QHostAddress>
#include <QUdpSocket>

class LocalMessenger : public QObject
{
    Q_OBJECT
public:
    explicit LocalMessenger(QObject *parent = nullptr);
public slots:
    void sendTextMessage(QString message);
    void setPort(int port);
    void setGroupAddress(QString address);
signals:
    void localMessageReceived(QString message);
private slots:
    void readDatagrams();
private:
    QUdpSocket udpSocket;
    QHostAddress groupAddress;
    int portNumber = 45454;

    void bind();
};

#endif // LOCALMESSENGER_H
