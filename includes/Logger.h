#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <qqml.h>
#include <QString>
class Logger : public QObject
{
    Q_OBJECT
    QML_ELEMENT
public:
    explicit Logger(QObject *parent = nullptr);
public slots:
    void log(QString message);
signals:
    void logged(QString message);
private:
};

#endif // LOGGER_H
