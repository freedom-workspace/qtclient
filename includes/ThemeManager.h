#ifndef THEMEMANAGER_H
#define THEMEMANAGER_H

#include <QObject>

class ThemeManager : public QObject
{
    Q_OBJECT
public:
    explicit ThemeManager(QObject *parent = nullptr);
public slots:
    void toggle();
    void set(QString theme);
signals:
private:
    QString m_current;
};

#endif // THEMEMANAGER_H
