in vec3 attr_pos;
uniform mat4 modelViewProjection;

out vec3 pos;

void main() {
    pos = attr_pos;
    pos.x += sin(time * 4.0 + pos.y) * amplitude;
    gl_Position = modelViewProjection * vec4(pos, 1.0);
}
