#include "includes/Crypto.h"
#include <QByteArray>
#include "botan/cryptobox.h"
#include "botan/pipe.h"
#include "botan/comp_filter.h"
#include "botan/base64.h"
#include "botan/buf_comp.h"
#include "botan/auto_rng.h"
#include "botan/symkey.h"
#include "botan/argon2.h"
#include "botan/passhash9.h"
#include <QCoreApplication>
#include <QSettings>
#include <QDebug>
#include <algorithm>
#include <QDir>

//TODO: Cleanup using imports and headers
using Botan::CryptoBox::decrypt;
using Botan::CryptoBox::encrypt;
using Botan::get_cipher;
using Botan::SymmetricKey;
using Botan::InitializationVector;
using Botan::Pipe;
using Botan::AutoSeeded_RNG;
using Botan::Base64_Encoder;
using Botan::Base64_Decoder;
using Botan::ENCRYPTION;
using Botan::DECRYPTION;
using std::string;
using Botan::argon2_check_pwhash;
using Botan::argon2_generate_pwhash;
using Botan::SecureVector;

Crypto::Crypto(QObject *parent) : QObject(parent)
{
    readOrGenerateAppKey();
}

Crypto::~Crypto()
{
    applicationKey = nullptr;
    m_password = "";
}

QString Crypto::encrypt(QString input)
{
    AutoSeeded_RNG rng;
    unsigned char encryptableContent[input.length()];
    std::memcpy(encryptableContent, input.toStdString().c_str(), input.length());
    std::string cypher;

    try{
        std::string localAppKey = applicationKey.toStdString();
        cypher = Botan::CryptoBox::encrypt(encryptableContent, input.length(), m_password.toStdString(), rng);
        localAppKey.clear();
    }
    catch(Botan::Exception &exception){
        qDebug() << exception.what();
    }

    rng.clear();
    return QString::fromStdString(cypher);
}

QString Crypto::decrypt(QString input)
{
    unsigned char decryptableContent[input.length()];
    std::memcpy(decryptableContent, input.toStdString().c_str(), input.length());
    std::string result;
    try{
        result = Botan::CryptoBox::decrypt(decryptableContent, input.length(), m_password.toStdString());
    }
    catch(Botan::Exception &exception){
        qDebug() << exception.what();
    }

    return QString::fromStdString(result);
}

QString Crypto::generatePasswordHash(QString input)
{
    AutoSeeded_RNG rng;
    const char * inputPtr = input.toStdString().c_str();
    std::string result = argon2_generate_pwhash(inputPtr, input.length(), rng, HashP, HashM, HashT);
    rng.clear();
    return QString::fromStdString(result);
}

bool Crypto::validatePasswordHash(QString entry, QString hash)
{
    return argon2_check_pwhash(
        entry.toStdString().c_str(),
        entry.length(),
        hash.toStdString());
}

void Crypto::readOrGenerateAppKey()
{
    QFile inputFile(QDir::current().path() + "/app.key");
    if(!inputFile.exists()){
        createAppKey(inputFile);
    }

    inputFile.open(QIODevice::ReadWrite);
    applicationKey = inputFile.readAll();
}

void Crypto::createAppKey(QFile &inputFile)
{
    //TODO: setup up local encryption for data?
    inputFile.open(QIODevice::ReadWrite);
    SecureVector<uint8_t> bytes;

    AutoSeeded_RNG rng;
    for(auto i = 0 ; i < ApplicationKeySize * KeyGenIterations; i++){
        bytes.push_back(rng.next_byte());
    }
    rng.clear();
    QByteArray appKey(reinterpret_cast<const char*>(bytes.data()), bytes.size());
    bytes.clear();
    inputFile.write(appKey);
    inputFile.close();
}

void Crypto::regenerateKey()
{
    QFile inputFile(QDir::current().path() + "/app.key");
    inputFile.remove();
    readOrGenerateAppKey();
}
