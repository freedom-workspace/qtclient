#include "includes/LocalMessenger.h"
LocalMessenger::LocalMessenger(QObject *parent)
    : QObject(parent),
    groupAddress("239.255.43.21")
{
    bind();
}

void LocalMessenger::sendTextMessage(QString message)
{
    QByteArray datagram = message.toLocal8Bit();
    udpSocket.writeDatagram(datagram, groupAddress, portNumber);
}

void LocalMessenger::setPort(int port)
{
    udpSocket.leaveMulticastGroup(groupAddress);
    portNumber = port;
    udpSocket.bind(QHostAddress::AnyIPv4, portNumber, QUdpSocket::ShareAddress);
}

void LocalMessenger::setGroupAddress(QString address)
{
    udpSocket.leaveMulticastGroup(groupAddress);
    groupAddress = QHostAddress(address);
    udpSocket.joinMulticastGroup(groupAddress);
}

void LocalMessenger::readDatagrams()
{
    QByteArray datagram;
    while (udpSocket.hasPendingDatagrams()) {
        datagram.resize(int(udpSocket.pendingDatagramSize()));
        udpSocket.readDatagram(datagram.data(), datagram.size());
        emit localMessageReceived(QString::fromLocal8Bit(datagram));
    }
}

void LocalMessenger::bind()
{
    udpSocket.bind(QHostAddress::AnyIPv4, portNumber, QUdpSocket::ShareAddress);
    udpSocket.joinMulticastGroup(groupAddress);
    connect(&udpSocket, &QUdpSocket::readyRead, this, &LocalMessenger::readDatagrams);
}
