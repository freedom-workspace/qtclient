#include "includes/Logger.h"

Logger::Logger(QObject *parent) : QObject(parent)
{

}

void Logger::log(QString message)
{
    emit logged(message);
}
