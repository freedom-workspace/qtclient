#include "includes/FileIO.h"
#include <QFile>
#include <QDebug>
#include <QTextStream>
const QString fileExtension= ".iodev";

void sanitize(QString& path)
{
    const QString beginString = "file://";
    if(path.startsWith(beginString)){
        path = path.remove(0, beginString.length());
    }
}
FileIO::FileIO(QObject *parent) : QObject(parent)
{

}

void FileIO::write(QString content, QString path)
{
    sanitize(path);
    path.append(fileExtension);
    QFile outputFile(path);
    outputFile.open(QIODevice::WriteOnly);
    QTextStream outStream(&outputFile);
    outStream << content;
    outputFile.close();

    emit fileWritten();
}

QString FileIO::read(QString path)
{
    sanitize(path);
    QString result;
    QFile file(path);

    if(file.exists() && file.open(QIODevice::ReadOnly)){
        auto content = file.readAll();
        result = QString::fromLocal8Bit(content);
    }

    emit fileRead(result);
    return result;
}
