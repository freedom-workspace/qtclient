/****************************************************************************
** Meta object code from reading C++ file 'Crypto.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "includes/Crypto.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Crypto.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Crypto_t {
    QByteArrayData data[15];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Crypto_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Crypto_t qt_meta_stringdata_Crypto = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Crypto"
QT_MOC_LITERAL(1, 7, 11), // "QML.Element"
QT_MOC_LITERAL(2, 19, 4), // "auto"
QT_MOC_LITERAL(3, 24, 13), // "regenerateKey"
QT_MOC_LITERAL(4, 38, 0), // ""
QT_MOC_LITERAL(5, 39, 11), // "setPassword"
QT_MOC_LITERAL(6, 51, 8), // "password"
QT_MOC_LITERAL(7, 60, 13), // "clearPassword"
QT_MOC_LITERAL(8, 74, 7), // "encrypt"
QT_MOC_LITERAL(9, 82, 5), // "input"
QT_MOC_LITERAL(10, 88, 7), // "decrypt"
QT_MOC_LITERAL(11, 96, 20), // "generatePasswordHash"
QT_MOC_LITERAL(12, 117, 20), // "validatePasswordHash"
QT_MOC_LITERAL(13, 138, 5), // "entry"
QT_MOC_LITERAL(14, 144, 4) // "hash"

    },
    "Crypto\0QML.Element\0auto\0regenerateKey\0"
    "\0setPassword\0password\0clearPassword\0"
    "encrypt\0input\0decrypt\0generatePasswordHash\0"
    "validatePasswordHash\0entry\0hash"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Crypto[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
       7,   16, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,

 // slots: name, argc, parameters, tag, flags
       3,    0,   51,    4, 0x0a /* Public */,
       5,    1,   52,    4, 0x0a /* Public */,
       7,    0,   55,    4, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
       8,    1,   56,    4, 0x02 /* Public */,
      10,    1,   59,    4, 0x02 /* Public */,
      11,    1,   62,    4, 0x02 /* Public */,
      12,    2,   65,    4, 0x02 /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,

 // methods: parameters
    QMetaType::QString, QMetaType::QString,    9,
    QMetaType::QString, QMetaType::QString,    9,
    QMetaType::QString, QMetaType::QString,    9,
    QMetaType::Bool, QMetaType::QString, QMetaType::QString,   13,   14,

       0        // eod
};

void Crypto::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Crypto *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->regenerateKey(); break;
        case 1: _t->setPassword((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->clearPassword(); break;
        case 3: { QString _r = _t->encrypt((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 4: { QString _r = _t->decrypt((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 5: { QString _r = _t->generatePasswordHash((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->validatePasswordHash((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Crypto::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Crypto.data,
    qt_meta_data_Crypto,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Crypto::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Crypto::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Crypto.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Crypto::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
