import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.11
import QtQuick.LocalStorage 2.0
import QtWebSockets 1.1
import QtQuick.Controls.Material 2.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Extras 1.4
import Qt.labs.settings 1.0
import "fonts/MaterialDesign.js" as Icons
import "scripts/localstorage.js" as Sockets
import "scripts/colors.js" as Colors
import QtQuick.LocalStorage 2.12
import QtQuick.Dialogs 1.2
import absence.dev.crypto 1.0

Rectangle{
    id: socketInfoSidePanel
    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
    Layout.fillHeight: true
    width: parent.width
    color: Material.background

    signal setProperties(var properties)

    onSetProperties: {
        nameTF.text = properties.name
        descriptionTF.text = properties.description || 'No Description Available'
        endpointTF.text = properties.endpoint
    }

    ColumnLayout{
        width: parent.width
        Layout.fillWidth: true
        Layout.fillHeight: true
        Label{
            text: "Endpoint Info"
            Layout.alignment: Qt.AlignHCenter
        }

        ColumnLayout{
            width: parent.width
            Label{
                text: "Name"
                Layout.fillWidth: true
                topPadding: 4
            }
            
            TextField{
                id: nameTF
                Layout.fillWidth: true
                Layout.maximumHeight: 50
                width: parent.width
            }
        }
        ColumnLayout{
            width: parent.width
            Label{
                text: "Description"
                Layout.fillWidth: true
                topPadding: 4
            }
            
            TextField{
                id: descriptionTF
                Layout.fillWidth: true
                Layout.maximumHeight: 50
                width: parent.width
            }
        }
        ColumnLayout{
            width: parent.width
            Label{
                text: "Endpoint"
                Layout.fillWidth: true
                topPadding: 4
            }
            
            TextField{
                id: endpointTF
                Layout.fillWidth: true
                Layout.maximumHeight: 50
                width: parent.width
            }
        }
        Button{
            text: "Update"
            Layout.fillWidth: true
        }
    }
}
