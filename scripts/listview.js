function repopulateSocketListView(model, items){
    model.clear()

    for(var item in items){
        model.append(item)
    }
}

function getCurrentModelForSocketListView(){
    var currentModel = socketListView.model
    return currentModel.get(socketListView.currentIndex)
}
