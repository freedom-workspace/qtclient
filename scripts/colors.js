function toColor(input, color){
    return ('<font color="%1">%2</font>').arg(color).arg(input)
}
