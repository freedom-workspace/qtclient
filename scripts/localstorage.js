function _getDbInstance(){
    var db = LocalStorage.openDatabaseSync("SocketsDB","","", 1000000)
    try{
        db.transaction(function(tx){
            tx.executeSql('CREATE TABLE IF NOT EXISTS sockets(id INTEGER PRIMARY KEY,name TEXT NOT NULL UNIQUE, description TEXT, endpoint TEXT NOT NULL, password TEXT)')
        })
    }
    catch(error){
        console.log('sql error', error)
    }

    return db
}

function getAllSockets(){
    var db = _getDbInstance()
    var rows = []
    db.transaction(function(tx){
        var statement = "SELECT name, description, endpoint, password FROM sockets"
        var results = tx.executeSql(statement)
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows.item(i)
            rows.push(row)
        }
    })
    return rows
}

function execute(input){
    var db = _getDbInstance()
    try{
        db.transaction(function(tx){
            var results = tx.executeSql(input)
            return results
        })
    }
    catch(error){
        console.log('storage error', error)
    }
}

function addFromJson(obj){
    var importObj = JSON.parse(obj)
    addSocket(importObj.name, importObj.description, importObj.endpoint, importObj.password, true)
}

function addSocket(name, description, endpoint, password, repopulateModel = true){
    var statement = "INSERT INTO sockets(name, description, endpoint, password) VALUES(?,?,?,?)"
    var db = _getDbInstance()
    try{
        db.transaction(function(tx){
            tx.executeSql(statement, [
                              name,
                              description,
                              endpoint,
                              crypto.generatePasswordHash(password)
                          ])
        })
    }
    catch(error){
        console.log('error', error)
    }
}

function removeSocket(socket, repopulateModel = true){
    var statement = "DELETE FROM sockets where name like '%%1%'".arg(socket.name)
    var db = _getDbInstance()
    try{
        db.transaction(function(tx){
            var results = tx.executeSql(statement)
            return results
        })
    }
    catch(error){
        console.log('error', error)
    }
}

function infoOnSocket(){
    var currentIndex = socketListView.currentIndex
    var currentModel = socketListView.model
    var selectedObject = currentModel.get(currentIndex)

    socketInfoDrawer.setProperties(selectedObject)
    socketInfoDrawer.open()
}
